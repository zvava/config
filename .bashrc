# ~z@zvava.org
# source global definitions
[[ -f /etc/bashrc ]] && source /etc/bashrc
[[ -f /etc/bash.bashrc ]] && source /etc/bash.bashrc

# aliases
[[ -f ~/.bashrc.aliases ]] && source ~/.bashrc.aliases

# homewrap
unset HOME REAL_HOME HOMEWRAP_HOME
export HOME=/home/zvava
export REAL_HOME=/home/zvava
export HOMEWRAP_HOME=/home/zvava/.local/home

# lang
[[ $LANG = '' ]] && export LANG=en_US.UTF-8

# include user scripts
[[ $PATH != *"/home/zvava/.local/bin"* ]] && export PATH="/home/zvava/.local/bin:$PATH"

# include pnpm scripts
export PNPM_HOME="/home/zvava/.local/share/pnpm"
[[ $PATH != *"$PNPM_HOME"* ]] && export PATH="$PNPM_HOME:$PATH"

# if not running interactively, stop
[[ $- != *i* ]] && return

# limits recursive functions
[[ -z "$FUNCNEST" ]] && export FUNCNEST=100

# prompt
PS1='$(printf "%*s\r%s" $(( COLUMNS+9 )) " \e[90m\w\e[0m " "  \e[97;1m\$\e[0m ")'
# title
PS1="$PS1\[\e]2;\u@\h\\a\]"

# use the up and down arrow keys for finding a command in history
# (you can write some initial letters of the command first)
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'
