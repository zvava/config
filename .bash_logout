# .bash_logout
# ~z@zvava.org

if [ "$SHLVL" = 1 ]; then
    [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
    rm -rf ~/.nv
    rm -rf ~/.java
    rm -f ~/.bash_history*
    rm -f ~/.node_repl_history*
    rm -f ~/.xsession-errors*
fi
