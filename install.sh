#!/bin/sh
# ~z@zvava.org

log() { echo "install.sh: $@"; }

running=$(whoami)
host=$(hostname)
cd "${0%/*}"
[ "$running" = root ] && log "running as superuser"

install_dotfiles() {
	log "installing to $1..."

	# copy configs
	cp -rf .config .local .scripts .nanorc .gitconfig* .X* .bash* $1/
	# make folders
	mkdir -p $1/Pictures/Screenshots
	mkdir -p $1/.config/nano/.backups
	# make scripts work
	chmod +x $1/.scripts/*
}

if [ -e "/data/data/com.termux/files/usr/bin/termux-info" ]; then
	log "termux detected"
	termuxfiles=/data/data/com.termux/files
	install_dotfiles $termuxfiles/home

	# copy etc
	log "installing /etc/..."
	cp -rf etc/* $termuxfiles/usr/etc/

	# fix shebangs
	termux-fix-shebang $termuxfiles/home/.scripts/*

	# fix bashrc
	sed -E 's/\/home\/(zvava|\$USER)/\/data\/data\/com.termux\/files\/home/g' .bashrc > $termuxfiles/home/.bashrc

	# fix nanorc
	sed 's/\/usr\/share/\/data\/data\/com.termux\/files\/usr\/share/'          .nanorc   > $termuxfiles/home/.nanorc
	echo "$(sed -E 's/^include .*nano-syntax-highlight.*$//' $termuxfiles/home/.nanorc)" > $termuxfiles/home/.nanorc

	# fix environment
	sed 's/\/home\/zvava/\/data\/data\/com.termux\/files\/home/g' etc/environment > $termuxfiles/usr/etc/environment

	# place remote hosts + ssh config
	sed -E 's/10.0.0.(88|89|90|91|92|93)/zvava.org/' etc/hosts   > $termuxfiles/usr/etc/hosts
	echo "$(sed 's/hostname/baby/'   $termuxfiles/usr/etc/hosts)" > $termuxfiles/usr/etc/hosts
	mkdir -p $termuxfiles/home/.ssh
	cp misc/remote-ssh-config $termuxfiles/home/.ssh/config
else
	unset u
	for u in /home/*; do
		user=$(echo $u | sed "s/\/home\///")
		install_dotfiles $u
		# fix bashrc
		sed -E "s/\/home\/(zvava|\\\$USER)/\/home\/$user/g" .bashrc > $u/.bashrc
		# remove nano-syntax-highlighting include if it doesn't exist
		[ -e /usr/share/nano-syntax-highlighting ] || sed -E 's/^include .*nano-syntax-highlight.*$//' .nanorc > $u/.nanorc
		# ensure file ownership
		chown $user:$user -R $u

		# remote ssh config
		if [ ! "$host" = chad ]; then
			log "creating remote ssh config"
			mkdir -p $u/.ssh
			cp misc/remote-ssh-config $u/.ssh/config
		fi
	done

	# full system install
	if [ "$running" = root ]; then
		install_dotfiles "/root"
		# fix bashrc
		sed -E 's/\/home\/(zvava|\$USER)/\/root/g' .bashrc > /root/.bashrc
		# remove nano-syntax-highlighting include if it doesn't exist
		[ -e /usr/share/nano-syntax-highlighting ] || sed -E 's/^include .*nano-syntax-highlight.*$//' .nanorc > /root/.nanorc
		# copy etc
		log "installing /etc/..."
		cp -rf etc/* /etc/

		# remote /etc/hosts & ssh config
		if [ ! "$host" = chad ]; then
			log "creating remote ssh config"
			mkdir -p /root/.ssh
			cp misc/remote-ssh-config /root/.ssh/config

			log "creating remote hosts file"
			sed -E 's/192.168.1.(88|89|90|91|92|93)/zvava.org/' etc/hosts   > /etc/hosts
			echo "$(sed "s/hostname/$host/"                  /etc/hosts)" > /etc/hosts
		fi
	fi
fi

log "done!"
