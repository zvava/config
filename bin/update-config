#!/bin/sh
# update dotfiles repository

configurations=(
	"autostart"
	"gtk-2.0" "gtk-3.0" "gtk-4.0"
	"xfce4" "i3" "sway"
	"dunst" "rofi" "picom.conf"
	"nano" ".nanorc-notes"
	# applications
	"neofetch" "kristall" "redshift" "easyeffects"
	# legacy
	"cava" "protonvpn"
)

homefiles=(
	".bash_logout" ".bash_profile" ".bashrc" ".bashrc.aliases"
	".Xresources" ".Xcompose"
	".gitconfig" ".nanorc"
)

[ -e ~/.config/config ] || git clone "ssh://git@zvava.org:2200/zvava/config.git"

# user scripts
rsync --recursive --fsync --delete ~/.local/bin ~/.config/config/

# vscode settings
if [ -e ~/.config/VSCodium ]; then
	mkdir -p ~/.config/config/.config/VSCodium/User/
	cp ~/.config/VSCodium/User/settings.json ~/.config/config/.config/VSCodium/User/
fi

# .desktop files
if [ -e ~/.local/share/applications ]; then
	mkdir -p ~/.config/config/.local/share/applications/
	rsync --recursive --fsync --delete ~/.local/share/applications ~/.config/config/.local/share\
		--exclude mimeinfo.cache --exclude wine --exclude games
fi

# misc configurations
for config in "${configurations[@]}"; do
	[ -e ~/.config/$config ] || { echo "skipping $config"; continue; }
	rsync -r --fsync --delete --force \
		~/.config/$config ~/.config/config/.config/ \
		--exclude launcher-23/* --exclude accels.scm --exclude .backups/*
done

# misc
for file in "${homefiles[@]}"; do
	[ -e ~/$file ] && cp ~/$file ~/.config/config/
done


# /etc
mkdir -p ~/.config/config/etc ~/.config/config/etc/lightdm ~/.config/config/etc/xdg/autostart ~/.config/config/etc/X11/
sed -E 's/(NTFY_TOKEN|HUE_TOKEN)=.*/\1=n\/a/' /etc/environment > ~/.config/config/etc/environment
sed -E "s/$HOSTNAME/hostname/" /etc/hosts > ~/.config/config/etc/hosts
cp /etc/lightdm/* ~/.config/config/etc/lightdm/
cp /etc/xdg/autostart/* ~/.config/config/etc/xdg/autostart/
cp -r /etc/X11/xorg.conf.d ~/.config/config/etc/X11/
cp -r /etc/X11/xinit ~/.config/config/etc/X11/
